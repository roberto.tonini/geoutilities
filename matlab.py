#!/bin/env python
"""
Collection of utilities to handle .mat binary files.
Several patches of the code have been derived from different discussions 
found in  Stack Overflow community at https://stackoverflow.com
"""
import os
import sys
import numpy as np
import scipy.io
import h5py

def load_mat(filename):
    """
    Main function which detects the verion of the .mat file.
     - .mat version < 7.3 is loaded with scipy
     - .mat version >= 7.3 is loaded with h5py (hdf5)
    """
    if os.path.exists(filename):
        try:
            f_mat = read_mat(filename)
            #print("Reading .mat file version < 7.3")
        except:
            try:
                f_mat = read_mat_v73(filename)
                #print("Reading .mat file version = 7.3")
            except:
                sys.exit("Unknown .mat file. Please check.")

    return f_mat

def read_mat_v73(filename):
    """
    The function loads .mat files version >= 7.3. It converts hdf5 objects 
    in python dictionaries. It calls the function _todict_v73() to convert
    all entries which are still hdf5 objects.
    """
    data = h5py.File(filename, 'r')
    return _todict_v73(data, "/") 

def _todict_v73(h5file, path):
    """
    A recursive function which constructs nested dictionaries from 
    hdf5-objects (Groups). Variable path is needed to explore hdf5 tree
    structure properly.
    """
    dictionary = {}
    for key, item in h5file[path].items():
        if "refs" in key:
            continue

        #print(key, type(item))
        if isinstance(item, h5py._hl.dataset.Dataset):
            type_tmp = item[...].dtype.kind
            if type_tmp == "f":
                # transpose needed to keep same dimensions of .mat (partial test)
                dictionary[key] = item[...].astype(type_tmp).transpose()
                #print(key, type_tmp, dictionary[key].shape)
            elif type_tmp == "u":
                dictionary[key] = item[...].astype("int")
            else:
                # a matlab cell of strings got me crazy! this is too slow, it needs optimization
                ntmp, nrows = np.shape(item[...])
                dictionary[key] = np.empty((nrows), dtype=str)
                for ic in range(nrows):
                    dictionary[key][ic] = ''.join(chr(c) for c in h5file[item[0,ic]])

        elif isinstance(item, h5py._hl.group.Group):
            #print(path + key + '/', h5file[path + key + '/'])
            dictionary[key] = _todict_v73(h5file, path + key + '/')

    return dictionary

def read_mat(filename):
    """
    The function loads .mat files version < 7.3. It converts matlab objects 
    in python dictionaries. It calls the function _check_keys to convert
    all entries which are still matlab objects.
    
    """
    data = scipy.io.loadmat(filename, struct_as_record=False, squeeze_me=True)
    return _check_keys(data)

def _check_keys(dictionary):
    """
    The function checks if entries in dictionary are matlab objects. If yes
    _todict is called to change them to nested dictionaries.
    """
    for key in dictionary:
        if isinstance(dictionary[key], scipy.io.matlab.mio5_params.mat_struct):
            dictionary[key] = _todict(dictionary[key])
    return dictionary

def _todict(matobj):
    """
    A recursive function which constructs nested dictionaries from 
    matlab objects
    """
    dictionary = {}
    for strg in matobj._fieldnames:
        elem = matobj.__dict__[strg]
        if isinstance(elem, scipy.io.matlab.mio5_params.mat_struct):
            dictionary[strg] = _todict(elem)
        else:
            dictionary[strg] = elem
    return dictionary

