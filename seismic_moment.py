#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np

def mw_to_m0(mw):
    """
    Calculate the seismic moment from the moment magnitude

    Input:
      - mw: moment magnitude
    Output:
      - m0: seismic moment (N*m)
    
    """
    m0 = np.power(10, (1.5*(mw+10.7333)))*1e-7
    return m0


def m0_to_mw(m0):
    """
    Calculate the moment magnitude from the seismic moment 

    Input:
      - m0: seismic moment (N*m)
    Output:
      - mw: moment magnitude
    
    """
    m0 = m0/1e-7  # conversion from N*m in dyne*cm   
    mw = 2.0/3.0 * np.log10(m0) - 10.7
    return mw


def m0_to_slip(length, width, m0, mu=None):
    """
    Calculate the homogeneous slip on a rectangular from seismic moment 
    and fault rectangular area
  
    Input: 
      - m0: seismic moment (N*m)
      - length: fault lenght (km)
      - width: fault width (km)
      - mu: rigidity (Pa)

    Output:
      - s: homogeneous slip (m)

    Unit conversion:
      - m0 = length * width * slip * mu 
      - (dyn*cm) = (cm) * (cm) * (cm) * (dyn*cm^-2)
      - 10^7*(N*m) = 10^5*(km) * 10^5*(km) * 10^2*(m) * 10*(Pa)
      - (N*m) = 10^6 * (km) * (km) * (m) * (Pa)
 
    """
    if mu is None:
        mu = 3e10

    s = m0/(mu*length*width*1e6)  
    return s


def size_to_m0(length, width, slip, mu=None):
    """
    Calculate the homogeneous slip on a rectangular from seismic moment 
    and fault rectangular area.
  
    Input: 
      - slip: homogeneous slip (m)
      - length: fault lenght (km)
      - width: fault width (km)
      - mu: rigidity (Pa)

    Output:
      - m0: seismic moment (N*m)

    Unit conversion:
      - m0 = length * width * slip * mu 
      - (dyn*cm) = (cm) * (cm) * (cm) * (dyn*cm^-2)
      - 10^7*(N*m) = 10^5*(km) * 10^5*(km) * 10^2*(m) * 10*(Pa)
      - (N*m) = 10^6 * (km) * (km) * (m) * (Pa)
 
    """
    if mu is None:
        mu = 3e10

    m0 = slip * length * width * mu * 1e6
    return m0  

