#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import numpy as np


def greens_law(depth_dw, depth_sw=None):
    """
    depth_dw: water depth in deep water, where one measures the offshore water
              elevation 
    depth_sw: water depth in shallow water, where one wants to apply the 
              Green's law amplification 

    If no depth_sw is provided, amplitude is calculated at 1 m depth 
    (i.e., approximatevely the coastline)
    """
    if depth_sw is None:
        depth_sw = 1.0

    if depth_sw < 1.0:
        sys.exit("Shallow depth must be positive and  >= 1.0")

    if depth_dw <= depth_sw:
        sys.exit("Shallow water depth ({0} m) can't be gretear than deep "
                 "water depth ({1} m)".format(depth_sw, depth_dw))

    amplification = np.power(depth_dw/depth_sw, 0.25)
    return amplification


def next_pow2(m):
    """
    Next power of 2 from m
    """
    n = 2
    while n < m: n = n * 2
    return n


def kajiura(z, h, dx):
    """
    Applies the Kajiura filter to a vertical seafloor displacement as
    transfer function for the corresponding surface water displacement 
    (i.e., tsunami initial condition). 
    
    This implementation is fast but with the approximation of assuming
    a single reference depth (average, maximum or whatever decide by the user) 
    for the whole displaced area.

    Input:
      - z: seafloor displacement as numpy array of dimension (nx, ny)
      - h: reference depth (m, positive values going deep) 
      - dx: spatial resolution (arc-min)
    Output:
      - zf: tsunami water displacement (filtered)
    """

    if h < 0.0:
        h = 0.0
        print("Warning: negative water depth is considered land", 
              "and converted to 0.0 m (no Kajiura effect).")

    min2m = 1852.0          # 1 arc-min in meters (approximation)
    fs = 1.0/(dx*min2m)     # spatial frequency (m)
    ny, nx = np.shape(z)

    NFFTx = 2^next_pow2(nx) 
    NFFTy = 2^next_pow2(ny)

    Y = np.fft.fft2(z, s=[NFFTx, NFFTy])
    fx = fs/2.*np.linspace(0., 1., int(0.5*NFFTx))
    fy = fs/2.*np.linspace(0., 1., int(0.5*NFFTy))
  
    f_twosidedx = np.hstack((fx, fx[::-1]))
    f_twosidedy = np.hstack((fy, fy[::-1]))
  
    xx = np.cosh(h*f_twosidedx*2.*np.pi)
    yy = np.cosh(h*f_twosidedy*2.*np.pi)
    denx, deny = np.meshgrid(xx, yy)
  
    kaj_filt = (Y/denx)/np.transpose(deny)
    p_filt = np.fft.ifft2(kaj_filt)
    zf = p_filt[:ny,:nx].real
  
    return zf


